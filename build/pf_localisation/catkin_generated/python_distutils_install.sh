#!/bin/sh

if [ -n "$DESTDIR" ] ; then
    case $DESTDIR in
        /*) # ok
            ;;
        *)
            /bin/echo "DESTDIR argument must be absolute... "
            /bin/echo "otherwise python's distutils will bork things."
            exit 1
    esac
    DESTDIR_ARG="--root=$DESTDIR"
fi

echo_and_run() { echo "+ $@" ; "$@" ; }

echo_and_run cd "/home/time/robotics/src/pf_localisation"

# ensure that Python install destination exists
echo_and_run mkdir -p "$DESTDIR/home/time/robotics/install/lib/python2.7/dist-packages"

# Note that PYTHONPATH is pulled from the environment to support installing
# into one location when some dependencies were installed in another
# location, #123.
echo_and_run /usr/bin/env \
    PYTHONPATH="/home/time/robotics/install/lib/python2.7/dist-packages:/home/time/robotics/build/lib/python2.7/dist-packages:$PYTHONPATH" \
    CATKIN_BINARY_DIR="/home/time/robotics/build" \
    "/usr/bin/python" \
    "/home/time/robotics/src/pf_localisation/setup.py" \
    build --build-base "/home/time/robotics/build/pf_localisation" \
    install \
    $DESTDIR_ARG \
    --install-layout=deb --prefix="/home/time/robotics/install" --install-scripts="/home/time/robotics/install/bin"
