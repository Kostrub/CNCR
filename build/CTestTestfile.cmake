# CMake generated Testfile for 
# Source directory: /home/time/robotics/src
# Build directory: /home/time/robotics/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs(gtest)
subdirs(ros_astra_launch)
subdirs(ex01_first_node)
subdirs(robot_tutorials)
subdirs(ros_astra_camera)
subdirs(pf_localisation)
