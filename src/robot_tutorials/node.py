#! /usr/bin/env python
# This basic node subscribes to incoming laser and sonar messages,
# decides what to do based on the incoming data, then publishes
# movement commands to the robot.

# Remember to comment your code so your team mates can understand it!

# @author Claudio Zito, Marco Becerra
import rospy
from geometry_msgs.msg import Twist
from sensor_msgs.msg import LaserScan


def callback(sensor_data):
    # sensor_data (LaserScan data type) has the laser scanner data
    # base_data (Twist data type) created to control the base robot
    base_data = Twist()
    base_data.angular.z = 0.1
    pub.publish(base_data)


if __name__ == '__main__':
    rospy.init_node('reactive_mover_node')
    rospy.Subscriber('base_scan', LaserScan, callback)
    pub = rospy.Publisher('cmd_vel', Twist, queue_size=100)
    rospy.spin()
