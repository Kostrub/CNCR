#!/usr/bin/env python

import rospy
from geometry_msgs.msg import Twist
from turtlesim.msg import Pose, Color

def callback_pose(data):
    pub = rospy.Publisher("/turtle1/cmd_vel", Twist, queue_size=10)
    vel_cmd = Twist()

    # vel_cmd.linear.x = vel_cmd.linear.x + 2
    # vel_cmd.linear.y = 0
    # vel_cmd.linear.z = 0
    #
    # vel_cmd.angular.x = 0
    # vel_cmd.angular.y = 0
    # vel_cmd.angular.z = 0

    #if data.x > 2 and data.x < 8:

    vel_cmd.linear.x = 1
    vel_cmd.linear.y = 0
    vel_cmd.linear.z = 0

    vel_cmd.angular.x = 0
    vel_cmd.angular.y = 0
    vel_cmd.angular.z = 0
    #print vel_cmd
    pub.publish(vel_cmd)

def call(po):
        pubpose = rospy.Publisher("/turtle/pose", Pose, queue_size=10)
        po = Pose()

        po.x = 5.5
        po.y = 5.5
        po.theta = 0
        po.linear_velocity = 0
        po.angular_velocity = 0
        pubpose.publish(po)

if __name__ == '__main__':
    rospy.init_node('listner', anonymous=True)
    rospy.Subscriber("/turtle1/pose", Pose, callback_pose, queue_size=10)
    rospy.Subscriber("/turtle1/cmd_vel", Twist, call, queue_size=10)

    while not rospy.is_shutdown():
        rospy.spin()
