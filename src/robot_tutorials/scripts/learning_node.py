#!/usr/bin/env python

import rospy
from math import sqrt
from nav_msgs.msg import Odometry
import robot_tutorials.msg


def distance(x1, y1, x2, y2):
    xd = x1 - x2
    yd = y1 - y2
    return sqrt(xd * xd + yd * yd)


class LandmarkMonitor(self, object):
    def __init__(self, pub, Landmarks):
        self.pub = pub
        self.landmarks = landmarks


def callback(self, msg):
    x = msg.pose.pose.position.x
    y = msg.pose.pose.position.y
    #rospy.loginfo('x: {}, y: {}'.format(x, y))
    closest_name = None
    closest_distance = None

    for l_name, l_x, l_y in self.landmarks:
        dist = distance(x, y, l_x, l_y)
        if closest_distance is None or dist < closest_distance:
            closest_name = l_name
            closest_distance = dist
    rospy.loginfo('closest: {}'.format(closest_name))


def main():
    rospy.init_node('learning_node')
    landmarks = []

    landmarks.append(('Cube', 0.31, -0.99))
    landmarks.append(('Dumpster', 0.11, -2.42))
    landmarks.append(('Cylinder', -1.14, -2.88))
    landmarks.append(('Barrier', 2.59, -0.83))
    landmarks.append(('Bookshelf', -0.09, 0.53))

    pub = rospy.Publisher('closest_landmark', LandmarkDistances)
    monitor = LandmarkMonitor(pub, landmarks)

    rospy.Subscriber('/odom', Odometry, monitor.callback)
    rospy.spin()


if __name__ == '__main__':
    main()
