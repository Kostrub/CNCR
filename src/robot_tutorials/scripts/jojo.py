import rospy
from std_msgs.msg import String, Int8


def jojo():
    hello_pub = rospy.Publisher("hello", String, queue_size=10)
    hello_pub_int = rospy.Publisher('hello_int', Int8, queue_size=10)
    hello_pub_loli = rospy.Publisher('hello_loli', String, queue_size=10)
    rospy.init_node('JOJO', anonymous=False)
    rate = rospy.Rate(10)
    counter = 0
    while not rospy.is_shutdown():
        greeting = "Hello, JOJO!"
        greeting_int = ord(greeting[counter % len(greeting)])
        greeting_loli = 'even' if greeting_int % 2 == 0 else 'odd'
        rospy.loginfo(greeting)
        hello_pub.publish(greeting)
        hello_pub_int.publish(greeting_int)
        hello_pub_loli.publish(greeting_loli)
        counter += 1
        rate.sleep()


if __name__ == '__main__':
    try:
        jojo()
    except rospy.ROSinterruptExeception:
        pass
